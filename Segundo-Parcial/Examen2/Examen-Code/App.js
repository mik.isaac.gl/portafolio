import { initializeApp } from "firebase/app";
import React from "react";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, onAuthStateChanged, signOut } from "firebase/auth";
import { StyleSheet, Text, View, Button, TextInput, ScrollView } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { useEffect } from "react";

const firebaseConfig = {
  apiKey: "AIzaSyDNr84469p31gi1XwWhNqFcWLWREl5IWao",
  authDomain: "loginfirebase-9f225.firebaseapp.com",
  projectId: "loginfirebase-9f225",
  storageBucket: "loginfirebase-9f225.appspot.com",
  messagingSenderId: "253277755286",
  appId: "1:253277755286:web:9946f69fd36164e101efb7"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const AuthScreen = ({email,password, setEmail, setPassword, isLogin, setIsLogin, handleAuthentication}) =>{
  return (
    <View style={styles.authContainer}>
      <Text style={styles.title}>{isLogin? 'Sign In' : 'Sing Up'}</Text>
      <TextInput 
        style={styles.input} 
        value={email}
        onChangeText={setEmail}
        placeholder="Email"
        autoCapitalize="none"
      />
      <TextInput 
        style={styles.input} 
        value={password}
        onChangeText={setPassword}
        placeholder="Password"
        secureTextEntry
      
      />
      <View style={styles.buttonContainer}>
        <Button 
          onPress={handleAuthentication} 
          title={isLogin? 'Sign In' : 'Sing Up' }
          color="#3498db"
        />
      </View>
      <View>
        <Text style={styles.toggleText}
          onPress={() => setIsLogin(!isLogin)}
        >
          {isLogin? 'Need an accound? Sign Up': 'Already have an accound? Sing In'}
        </Text>
      </View>
    </View>
  );
}

const AuthenticatedScreen = ({user, handleAuthentication}) => {
  return (
    <View style={styles.authContainer}>
      <Text style={styles.title}>WELCOME</Text>
      <Text style={styles.emailText}>{user.email}</Text>
      <Button title="Log Out" onPress={handleAuthentication} color="#74c3c"/>
    </View>
  );
}



export default function App() {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [user, setUser] = React.useState(null);
  const [isLogin, setIsLogin] = React.useState(true);

  const auth = getAuth(app)
  useEffect(() => {
    const unsuscribe = onAuthStateChanged(auth, (user)=>{
      setUser(user);
    });
    return () => unsuscribe();
  }, [auth])

  const handleAuthentication = async () => {
    try {
      if (user) {
        console.log('User logged out successfully')
        await signOut(user);
      } else {
        if (isLogin) {
          await signInWithEmailAndPassword(auth, email, password);
          console.log('User logged in successfully')
        } else {
          await createUserWithEmailAndPassword(auth, email, password);
          console.log('User created successfully')
        }
      }
    } catch (error) {
      console.error('Authentication error: '+error);
    }
  }
  
  return (
    <ScrollView contentContainerStyle={styles.container}>
      {user? (
        <AuthenticatedScreen user={user} handleAuthentication={handleAuthentication} />
      ) : (
        <AuthScreen 
          email={email}
          password={password}
          isLogin={isLogin}
          setEmail={setEmail}
          setPassword={setPassword}
          setIsLogin={setIsLogin}
          handleAuthentication={handleAuthentication}
        />
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    backgroundColor: '#f0f0f0',
  },
  authContainer: {
    width: '80%',
    maxWidth: 400,
    backgroundColor: '#fff',
    padding: 16,
    borderRadius: 8,
    elevation: 3,
  },
  title: {
    fontSize: 24,
    marginBottom: 16,
    textAlign: 'center',
  },
  input: {
    height: 40,
    borderColor: '#ddd',
    borderWidth: 1,
    marginBottom: 16,
    padding: 8,
    borderRadius: 4,
  },
  buttonContainer: {
    marginBottom: 16,
  },
  toggleText: {
    color: '#3498db',
    textAlign: 'center',
  },
  bottomContainer: {
    marginTop: 20,
  },
  emailText: {
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 20,
  },
});