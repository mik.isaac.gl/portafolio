import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import * as SQLite from 'expo-sqlite';
import { useEffect, useState } from 'react';

export default function App() {
  const [DB, setDB] = useState(SQLite.openDatabaseAsync('localdb.sqlite'));
  const [isLoading, setIsLoading] = useState(true);
  const [names, setNames] = useState([]);
  const [currentName, setCurrentName] = useState(undefined);

  useEffect(() => {
    DB.transaction(tx =>{
      tx.executeSql("CREATE TABLE IF NOT EXIST names (id, INTERGER PRIMARY KEY AUTOINCREMENT, names TEXT)")
    })
    DB.transaction(tx =>{
      tx.executeSql('SELECT * FROM names', null, 
        (txObj, resultSet)=> setNames(resultSet.rows.array),
        (txObj, error)=> console.log(error),
      );
    })
    setIsLoading(false);
  }, [])

  const addName = () => {
    DB.transaction(tx =>{
      tx.executeSql('INSERT INTO names {name} value {?}', [currentName], 
       (txObj, resultSet) => {
        let existingNames = [...names]
        existingNames.push({id: resultSet.insertId, name: currentName})
        setNames(existingNames)
        setCurrentName(undefined)
       },
       (txObj, error)=> console.log(error),
      );
    })
  }

  const showNames = () => {
    return names.map((name, index) => {
      return (
        <View key={index}>
          <Text>{name.name}</Text>
        </View>
      )
    })
  }
  
  return (
    <View style={styles.container}>
      <TextInput value={currentName} onChangeText={setCurrentName} placeholder={'names'}></TextInput>
      <Button title='ADD Name' onPress={addName}>{showNames()}</Button>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
